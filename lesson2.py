from pprint import pprint
print = pprint


# lst1 = list(1) TODO ERROR!
# print(lst1)

# lst.sort()

# print(lst[2])
# lst[2] = 12
# print(lst[-1])

# a = [1, 2, 3]
# b = a.copy()
# a[1] = 22
# print(b)
# lst.reverse()
# print(lst[::-1])

# *other, end = lst
# print(end)

# some_lst = range(2, 101, 2)
# print(list(some_lst))

# lst.append(10)
# print(lst)

# lst.insert(3, 'Hello')
# print(lst)

# lst.remove(2)
# print(lst)

# print(lst.count(2))

# print(lst.pop())
# print(lst.pop(0))
# print(lst)

# print(lst.index(2))
mtx = [
    [2, 2, 2],
    [0],
    [1, 2, 2, 4, 5, 6],
    [0, 1, 2, 3],
]

# mtx.sort(key=lambda x: x.count(2))
# mtx = sorted(mtx, key=lambda x: x.count(2), reverse=True)
# print(mtx)
# mtx[2][2] = 3
# print(mtx)


# tpl = (1, 0, [1], 16)
# tpl = tpl + (1, )
# # tpl[1][0] = 10
# print(tpl)

# st = {2, 3, 3, 4, 2, 1, 4}
#
# st.add(5)
# print(st.pop())
# print(st)
# print(list(set(lst)))


greet = "hello world! my  name is loki"

# greet = greet.replace('  ', ' ')
# print(greet.count(' ') + 1)
#
# fructs = ['banana', 'orange', 'apple']
#
# print(' =^_^= '.join(fructs))
# greet = "Hello"
#
# print(greet.center(100, '='))


# class A:
#
#     def __hash__(self):
#         return 1


user = {
    'name': "Loki",
    "cats": ['Thor', 'Dolka', 'Kivi'],
    "age": 21,
    # A(): 21
    # 12: "dvenadcat",
    # (1, 2, 3): 'T_T',
    # []: 123
    # {}
}

print(user.items())

# print(user[(1, 2, 3)])

# lst = [
#     1, 2, 2.1, 0.1, 4, 6, 2, 7, 11
# ]
# print(list(dict.fromkeys(lst)))
